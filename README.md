# Carte Noire

A simple Hugo theme for blogging. Not named after the coffee.

![Homepage](http://i.imgur.com/xlmHArV.png)

### Article
![Article](http://i.imgur.com/8rD8FfC.png)

### Posts grouped by year
![All Posts](http://i.imgur.com/9bNs2Sc.png)

### JavaScript Search
![Search](http://i.imgur.com/yQqMeSl.png)

### Menu by mmenu
![Menu](http://i.imgur.com/SClrNSH.png)

## Contact
If you wish to contact me regarding this theme please raise an issue on GitLab
or email me [francisco@oblita.com](mailto:francisco@oblita.com).

## Contribution
Pull requests are very welcome.

## Theme
This Hugo theme is a port of the Jekyll theme of the same name. Ideas and inspiration are taken from other places but the code is my own.

## Tools and Libraries
The following tools and libraries are used in this theme

### JavaScript
 * [jQuery](http://jquery.com/)
 * [MMenu](http://mmenu.frebsite.nl/)
 * [HighlightJS](https://highlightjs.org/)
 * [Waypoints](http://imakewebthings.com/waypoints/)
 * [Simple Jekyll Search](https://github.com/christian-fei/Simple-Jekyll-Search)

### CSS
 * [Bootstrap](http://getbootstrap.com/)
 * [Font Awesome](http://fortawesome.github.io/Font-Awesome/)

### Social
 * [AddThis](http://www.addthis.com/)

### Typesetting
 * [KaTeX](https://katex.org/)
   For pages containing math, [Mmark][] files are preferred. There's more about that on [this blog post][nosubstance].

[mmark]: https://mmark.miek.nl/syntax
[nosubstance]: http://nosubstance.me/post/a-great-toolset-for-static-blogging/

### Other
 * [Real Favicon Generator](http://realfavicongenerator.net/)

## License
The theme, HTML, CSS and JavaScript is licensed under GPLv3 (unless stated otherwise in the file).
